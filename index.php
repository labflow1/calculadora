<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Operaciones Básicas</title>
    <link rel="stylesheet" href="styles.css"> <!-- Práctica 1: Separación de estilos -->
</head>
<body>
    <div class="container">
        <h1>Operaciones Básicas</h1>
        <h2>Luis Fernando Guillen Gonzalez</h2>
        <h3>Grado: 10</h3>
        <h3>Grupo: A</h3>
        
        <form action="" method="POST" class="calculator">
            <div class="input-group">
                <label for="valor1">Valor 1:</label>
                <input type="number" id="valor1" name="valor1" required>
            </div>

            <div class="input-group">
                <label for="valor2">Valor 2:</label>
                <input type="number" id="valor2" name="valor2" required>
            </div>

            <div class="input-group-radio">
                <div>
                    <input type="radio" id="suma" name="operacion" value="suma" required>
                    <label for="suma">Suma</label>
                </div>
                <div>
                    <input type="radio" id="resta" name="operacion" value="resta" required>
                    <label for="resta">Resta</label>
                </div>
            </div>

            <input type="submit" value="Calcular">
        </form>

        <!-- Respuesta -->
        <div class="result-container">
            <?php
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $valor1 = filter_input(INPUT_POST, 'valor1', FILTER_VALIDATE_INT); // Práctica 2: Validación de entrada
                $valor2 = filter_input(INPUT_POST, 'valor2', FILTER_VALIDATE_INT); // Práctica 2: Validación de entrada
                $operacion = htmlspecialchars(trim($_POST['operacion'])); // Práctica 3: Escapado de caracteres especiales

                if ($valor1 !== false && $valor2 !== false) {
                    if ($operacion === 'suma' || $operacion === 'resta') { // Práctica 4: Validación de datos esperados
                        if ($operacion === 'suma') {
                            $resultado = $valor1 + $valor2;
                            echo "<h4>Resultado de la Suma</h4>";
                        } else {
                            $resultado = $valor1 - $valor2;
                            echo "<h4>Resultado de la Resta</h4>";
                        }
                        echo "<p>Valor 1: $valor1</p>";
                        echo "<p>Valor 2: $valor2</p>";
                        echo "<p>Resultado: $resultado</p>";
                    } else {
                        echo "Operación no válida.";
                    }
                } else {
                    echo "Los valores deben ser números enteros.";
                }
            }
            ?>
        </div>
        <!-- Fin Respuesta -->

    </div>
</body>
</html>
